import { Container, Grid, GridItem, Heading } from "@chakra-ui/react";
import { FC, useState } from "react";
import { DateTime } from "luxon";
import { getRelevantScheduleEntries } from "./utils/dataHelpers";
import data from "./utils/data.json";

const App: FC = () => {
  const [selectedDate, setSelectedDate] = useState<string>(DateTime.now().toFormat("yyyy'-'MM'-'dd"));

  const relevantScheduleEntries = getRelevantScheduleEntries(data.events);

  return (
    <Container
      padding="2rem"
      display="flex"
      flexDirection="column"
      alignItems="center"
    >
      <Heading>Calendar</Heading>
      <input type="date" value={selectedDate} onChange={e => {
        setSelectedDate(e.target.value);
      }} />
      <Grid templateColumns="repeat(3, 0.5fr)" maxWidth={500} paddingTop="2rem">
        {Array(24).fill("").map((_, i) => {
          return (
            <>
              <GridItem
                borderLeft="1px"
                borderRight="1px"
                borderTop="1px"
                padding="0.5rem 1rem"
                textAlign="right"
                rowSpan={4}
                colSpan={2}
              >
                {i}:00 - {i + 1}:00
              </GridItem>
              <GridItem
                borderTop="1px"
                borderRight="1px"
                padding="0.5rem 1rem"
                rowSpan={4}
              >
                Eztet
              </GridItem>
            </>
          )
        })}
      </Grid>
    </Container >
  );
}

export default App;
