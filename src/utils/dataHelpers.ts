import { DateTime } from "luxon";
import { RelevantSchedule, Schedule } from "./types";

// We don't need every data for the calendar view so might as well drop them
export const getRelevantScheduleEntries = (
  entries: Schedule[]
): RelevantSchedule[] =>
  entries.map(({ id, start, end, color }) => ({ id, start, end, color }));

// Compare two objects with RelevantSchedule-ish types by their start time
export function compareStartDateTime<Type extends RelevantSchedule>(
  a: Type,
  b: Type
): number {
  const d1 = DateTime.fromISO(a.start);
  const d2 = DateTime.fromISO(b.start);

  if (d1 < d2) {
    return -1;
  } else if (d1 > d2) {
    return 1;
  } else {
    return 0;
  }
}

// Returns an ordered array of RelevantSchedule-ish objects by their start time
export function orderSchedulesByStartTime<Type extends RelevantSchedule>(
  inputArr: Type[]
): Type[] {
  return inputArr.sort(compareStartDateTime);
}
