import {
  getRelevantScheduleEntries,
  compareStartDateTime,
} from './dataHelpers';
import { exampleScheduleArray, expectedScheduleReducedArray, exampleBadOrderScheduleArray } from './mockData';


test("getRelevantScheduleEntries passes", () => {
  expect(getRelevantScheduleEntries(exampleScheduleArray)).toMatchObject(expectedScheduleReducedArray);
})

test("compareStartDateTime first and second element matches", () => {
  expect(compareStartDateTime(exampleBadOrderScheduleArray[0], exampleBadOrderScheduleArray[1])).toBe(-1);
})

test("compareStartDateTime second and third element matches", () => {
  expect(compareStartDateTime(exampleBadOrderScheduleArray[1], exampleBadOrderScheduleArray[2])).toBe(1);
})