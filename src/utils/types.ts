export interface RelevantSchedule {
  id: number;
  start: string;
  end: string;
  color: string;
}

export interface Schedule extends RelevantSchedule {
  title?: string;
  registered?: boolean;
  location?: string;
}
