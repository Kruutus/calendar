export const exampleScheduleArray = [
  {
    id: 1,
    start: "2021-07-26T23:00:00",
    end: "2021-07-27T02:00:00",
    title: "Aufidio, praetorio, erudito homine, oculis capto, saepe audiebam.",
    color: "support2",
    registered: true,
  },
  {
    id: 2,
    start: "2021-07-27T08:00:00",
    end: "2021-07-27T08:15:00",
    title: "Cum se lucis magis quam utilitatis desiderio moveri diceret.",
    color: "support1",
    location: "Moscow",
  },
];

export const expectedScheduleReducedArray = [
  {
    id: 1,
    start: "2021-07-26T23:00:00",
    end: "2021-07-27T02:00:00",
    color: "support2",
  },
  {
    id: 2,
    start: "2021-07-27T08:00:00",
    end: "2021-07-27T08:15:00",
    color: "support1",
  },
];

export const exampleBadOrderScheduleArray = [
  {
    id: 1,
    start: "2021-07-26T23:00:00",
    end: "2021-07-27T02:00:00",
    color: "support2",
  },
  {
    id: 2,
    start: "2021-07-27T08:00:00",
    end: "2021-07-27T08:15:00",
    color: "support1",
  },
  {
    id: 3,
    start: "2021-07-27T04:00:00",
    end: "2021-07-27T05:00:00",
    color: "support2",
  },
];
